package ru.vpavlova.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.vpavlova.tm.event.ConsoleEvent;
import ru.vpavlova.tm.listener.AbstractListener;

@Component
public class CommandsListListener extends AbstractListener {

    @NotNull
    @Autowired
    private AbstractListener[] listeners;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "commands";
    }

    @NotNull
    @Override
    public String description() {
        return "Show program commands.";
    }

    @Override
    @EventListener(condition = "@commandsListListener.name() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        System.out.println("[COMMANDS]");
        for (@NotNull final AbstractListener listener : listeners) {
            if (listener.name() != null) {
                System.out.println(listener.name() + " - " + listener.description());
            }
        }
    }

}
